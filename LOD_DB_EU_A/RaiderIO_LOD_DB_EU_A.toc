## Interface: 100002
## Author: cqwrteur
## Title: Raider.IO LOD Database EU M+ Alliance
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 3
## X-RAIDER-IO-LOD-FACTION: Alliance
## X-RAIDER-IO-LOD-REQUIRE-RIO: 1

../RaiderIO/db/db_eu_alliance_characters.lua
../RaiderIO/db/db_eu_alliance_lookup.lua
