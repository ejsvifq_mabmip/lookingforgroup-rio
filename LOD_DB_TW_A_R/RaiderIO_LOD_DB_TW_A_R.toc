## Interface: 100002
## Author: cqwrteur
## Title: Raider.IO LOD Database TW Raid Alliance
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 4
## X-RAIDER-IO-LOD-FACTION: Alliance
## X-RAIDER-IO-LOD-REQUIRE-RIO: 1

../RaiderIO/db/db_raiding_tw_alliance_characters.lua
../RaiderIO/db/db_raiding_tw_alliance_lookup.lua
