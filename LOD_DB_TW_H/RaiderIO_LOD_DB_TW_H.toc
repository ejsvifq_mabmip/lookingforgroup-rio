## Interface: 100002
## Author: cqwrteur
## Title: Raider.IO LOD Database TW M+ Horde
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 4
## X-RAIDER-IO-LOD-FACTION: Horde
## X-RAIDER-IO-LOD-REQUIRE-RIO: 1

../RaiderIO/db/db_tw_horde_characters.lua
../RaiderIO/db/db_tw_horde_lookup.lua
