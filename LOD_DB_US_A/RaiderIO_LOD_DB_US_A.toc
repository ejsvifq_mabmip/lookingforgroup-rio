## Interface: 100002
## Author: cqwrteur
## Title: Raider.IO LOD Database US M+ Alliance
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 1
## X-RAIDER-IO-LOD-FACTION: Alliance
## X-RAIDER-IO-LOD-REQUIRE-RIO: 1

../RaiderIO/db/db_us_alliance_characters.lua
../RaiderIO/db/db_us_alliance_lookup.lua
