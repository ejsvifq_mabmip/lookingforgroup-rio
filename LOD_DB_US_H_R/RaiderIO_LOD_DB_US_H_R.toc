## Interface: 100002
## Author: cqwrteur
## Title: Raider.IO LOD Database US Raid Horde
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 1
## X-RAIDER-IO-LOD-FACTION: Horde
## X-RAIDER-IO-LOD-REQUIRE-RIO: 1

../RaiderIO/db/db_raiding_us_horde_characters.lua
../RaiderIO/db/db_raiding_us_horde_lookup.lua
